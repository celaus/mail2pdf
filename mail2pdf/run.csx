#r "Microsoft.Graph"
using Microsoft.Graph;
using System.Net;
using System.IO.MemoryStream;
using IronPdf;


// or System.IO.MemoryStream PdfStream = PDF.GetStream;
// or byte[] PdfBinary = PDF.GetBinary;

public static async Task Run(Message msg, TraceWriter log, IAsyncCollector<byte[]> onedriveFile)  
{
    log.Info("Microsoft Graph webhook trigger function processed a request.");

    if (msg.Subject.Contains("convert") && msg.From.Equals(msg.Sender)) {
        log.Info($"Processed email: {msg.BodyPreview}");

  
        HtmlToPdf HtmlToPdf = new IronPdf.HtmlToPdf();
        PdfResource PDF = HtmlToPdf.RenderHtmlAsPdf(msg.Body.content);
        pdfOutput = PDF.GetBinary;
        await onedriveFile.AddAsync(pdfOutput);
        return;
    }
}